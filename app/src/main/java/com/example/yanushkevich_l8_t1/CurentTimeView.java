package com.example.yanushkevich_l8_t1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.yanushkevich_l8_t1.R;

import java.util.Calendar;

public class CurentTimeView extends View {

    private int heightElements;
    private int widht;
    private Paint paint;

    public CurentTimeView(Context context) {
        super(context);
    }

    public CurentTimeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private void drawSeconds(Canvas canvas) {
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
        int secondsWidth = widht*5;
        int height = heightElements - (seconds * ((heightElements-widht) / 60));
        int color = getResources().getColor(R.color.secondsColor);
        drawRectangle(canvas, height, secondsWidth, color);
        drawTime(canvas, seconds, secondsWidth);
        drawTimeText(canvas, secondsWidth, "Seconds");
    }

    private void drawMinutes(Canvas canvas) {
        Calendar c = Calendar.getInstance();
        int minutes = c.get(Calendar.MINUTE);
        int height = heightElements - (minutes * ((heightElements-widht) / 60));
        int minutesWidth = widht * 3;
        int color = getResources().getColor(R.color.minutesColor);
        drawRectangle(canvas, height, minutesWidth, color);
        drawTime(canvas, minutes, minutesWidth);
        drawTimeText(canvas, minutesWidth, "Minutes");
    }

    private void drawHourse(Canvas canvas) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int height = heightElements - (hour * ((heightElements-widht) / 24));
        int hoursWidth = widht;
        int color = getResources().getColor(R.color.hoursColor);
        drawRectangle(canvas, height, hoursWidth, color);
        drawTime(canvas, hour, hoursWidth);
        drawTimeText(canvas, hoursWidth, "Hours");
    }

    private void drawRectangle(Canvas canvas, int height, int width, int color) {
        paint = new Paint();
        paint.reset();
        paint.setColor(color);
        paint.setStrokeWidth(5);
        canvas.drawRect(width, heightElements, width + widht, height, paint);
    }

    private void drawTime(Canvas canvas, int time, int width){
        String text = "" + time;
        paint.setTextSize(60);
        paint.setColor(Color.BLUE);
        paint.setTextAlign(Paint.Align.CENTER);
        int widthX = width+widht/2;
        int heightY = heightElements - widht/5;
        canvas.drawText(text, widthX, heightY, paint);
    }

    private void drawTimeText(Canvas canvas, int width, String text){
        paint.setTextSize(60);
        paint.setColor(Color.BLACK);
        paint.setTextAlign(Paint.Align.CENTER);
        int widthX = width+widht/2;
        int heightY = widht/2;
        canvas.drawText(text, widthX, heightY, paint);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        widht = canvas.getWidth()/ 7;
        heightElements = canvas.getHeight();
        canvas.drawARGB(80, 102, 204, 255);
        drawHourse(canvas);
        drawMinutes(canvas);
        drawSeconds(canvas);
        invalidate();
    }

}
